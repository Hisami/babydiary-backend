DROP TABLE IF EXISTS user;
DROP TABLE IF EXISTS baby_profile;
DROP TABLE IF EXISTS comment;
DROP TABLE IF EXISTS icon;
DROP TABLE IF EXISTS entry;
CREATE TABLE user (
    id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
    name VARCHAR(100),
    email VARCHAR(150),
    password VARCHAR(255),
    role VARCHAR(30),
    imgUrl VARCHAR(350)
);

CREATE TABLE baby_profile (
    id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
    name VARCHAR(100),
    birthDate DATETIME,
    imgUrl VARCHAR(350),
    description VARCHAR(600),
    id_user INT,
    Foreign Key (id_user) REFERENCES user(id) ON DELETE CASCADE
);

CREATE TABLE comment (
    id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
    createdAt DATETIME,
    imgUrl VARCHAR(350),
    comment VARCHAR(1000),
    id_baby INT,
    Foreign Key (id_baby) REFERENCES baby_profile(id) ON DELETE CASCADE
);

CREATE TABLE icon (
    id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
    name VARCHAR(350),
    explication VARCHAR(350)
);

CREATE TABLE entry (
    id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
    id_icon INT,
    Foreign Key (id_icon) REFERENCES icon(id) ON DELETE SET NULL,
    id_baby INT,
    Foreign Key (id_baby) REFERENCES baby_profile(id) ON DELETE CASCADE,
    createdAt DATETIME,
    description VARCHAR(350)
);

INSERT INTO user (name,email,password,role, imgUrl) VALUES 
("User1","User1@user1.com","1234","'ROLE_USER'","imgurl1"),("User2","User2@user2.com","1234","'ROLE_USER'","imgurl2"),("User3","User3@user3.com","1234","'ROLE_USER'","imgurl3");

INSERT INTO baby_profile (name,birthDate,imgUrl,description,id_user) VALUES 
("baby1",'2023-02-01',"imagebaby1","this is description of baby1",1),("baby2",'2023-03-01',"imagebaby2","this is description of baby2",2),("baby3",'2023-04-01',"imagebaby3","this is description of baby3",3),("baby4",'2023-04-01',"imagebaby4","this is description of baby4",3);

INSERT INTO comment (createdAt,imgUrl,comment,id_baby) VALUES 
('2023-08-01',"imgurl1","This is a comment1 for baby1",1),('2023-08-02',"imgurl2","This is a comment2 for baby1",1),('2023-08-03',"imgurl3","This is a comment3 for baby1",1);

INSERT INTO icon (name, explication) VALUES 
("Milk","This is a milk icon"),("meal","This is a meal icon"),("BigToilet","This is a big toilet icon"),("SmallToilet","This is a small toilet icon"),("SmallToilet","This is a small toilet icon"),("Sleep","This is a sleep icon"),("WakeUp","This is a wake up icon"),("Wakeup","This is a wakeup icon");

INSERT INTO entry (id_icon,id_baby,createdAt,description) VALUES 
(1,1,'2023-08-01 10:20:00',"He finished a bottle of milk."),(1,1,'2023-08-01 10:50:00',"He finished a bottle of milk.");

SELECT *, b.id b_id, b.name b_name, b.imgUrl b_img, u.id u_id, u.name u_name,u.imgUrl u_img FROM baby_profile b LEFT JOIN user u ON b.id_user = u.id;

SELECT *, b.id b_id, b.name b_name, b.imgUrl b_img, u.id u_id, u.name u_name,u.imgUrl u_img FROM baby_profile b LEFT JOIN user u ON b.id_user = u.id WHERE id_user= 1;

UPDATE baby_profile SET name="updateBabyName",imgUrl="updateImg",birthDate="2023-02-01",description="updateDescription",id_user=1 WHERE id=1;

DELETE FROM baby_profile WHERE baby_profile.id = 5;

SELECT *,c.id c_id, c.imgUrl c_img,b.id b_id, b.imgUrl b_img FROM comment c LEFT JOIN baby_profile b ON c.id_baby = b.id;

SELECT * FROM icon ;

SELECT *,e.id e_id,e.description e_des, b.id b_id,b.description b_des,b.name b_name, i.id i_id, i.name i_name FROM entry e LEFT JOIN baby_profile b ON e.id_baby = b.id LEFT JOIN icon i ON e.id_icon = i.id;

SELECT * FROM user WHERE email="User1@user1.com";