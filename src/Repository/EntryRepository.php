<?php

namespace App\Repository;

use App\Entities\Entry;
use App\Entities\BabyProfile;
use App\Entities\Icon;
use DateTime;
use PDO;

class EntryRepository {
    private $connection;
    public function __construct() {
        $this->connection = Connection::getConnection();;
    } 

    public function sqlToBabyProfile(array $result): ?BabyProfile {
        if($result){
            $babyProfile = new BabyProfile();
            $babyProfile->setId($result['b_id']);
            $babyProfile->setName($result['b_name']);
            $babyProfile->setImgUrl($result['imgUrl']);
            $babyProfile->setBirthDate(new DateTime($result['birthDate']));
            $babyProfile->setDescription($result['b_des']);
            return $babyProfile;
        }else{
            return null;
        }
    }

    public function sqlToIcon(array $result): ?Icon {
        if($result){
            $icon = new Icon();
            $icon->setId($result['i_id']);
            $icon->setName($result['i_name']);
            $icon->setExplication($result['explication']);
            return $icon;
        }else{
            return null;
        }
    }

    /**
     * @return Entry[]
     */
    public function findAll():?array {
        $list = [];
        $statement = $this->connection->prepare('SELECT *,e.id e_id,e.description e_des, b.id b_id,b.description b_des,b.name b_name, i.id i_id, i.name i_name FROM entry e LEFT JOIN baby_profile b ON e.id_baby = b.id LEFT JOIN icon i ON e.id_icon = i.id;');
        $statement->execute();
        foreach($statement->fetchAll() as $line) {
            $entry = new Entry();
            $entry->setId($line['e_id']);
            $entry->setCreatedAt(new DateTime($line['createdAt']));
            $entry->setDescription($line['e_des']);
            $entry->setBabyProfile($this->sqlToBabyProfile($line));
            $entry->setIcon($this->sqlToIcon($line));
            $list[] = $entry;
        }
        return $list;
    }
    
    /**
     * @return Entry
     */
    public function findById(int $id):?Entry{
    $statement = $this->connection->prepare('SELECT *,e.id e_id,e.description e_des, b.id b_id,b.description b_des,b.name b_name, i.id i_id, i.name i_name FROM entry e LEFT JOIN baby_profile b ON e.id_baby = b.id LEFT JOIN icon i ON e.id_icon = i.id WHERE e.id= :id;');
    $statement->bindValue('id', $id);
    $statement->execute();
    $result = $statement ->fetch();
    if($result){
        $entry = new Entry();
        $entry->setId($result['e_id']);
        $entry->setCreatedAt(new DateTime($result['createdAt']));
        $entry->setDescription($result['e_des']);
        $entry->setBabyProfile($this->sqlToBabyProfile($result));
        $entry->setIcon($this->sqlToIcon($result));
        return $entry;
    }
        return null;
    }

    /**
     * @return Entry[]
     */
    public function findByBabyId(int $id):?array {
        $list = [];
        $statement = $this->connection->prepare('SELECT *,e.id e_id,e.description e_des, b.id b_id,b.description b_des,b.name b_name, i.id i_id, i.name i_name FROM entry e LEFT JOIN baby_profile b ON e.id_baby = b.id LEFT JOIN icon i ON e.id_icon = i.id WHERE b.id= :id;');
        $statement->bindValue('id', $id);
        $statement->execute();
        foreach($statement->fetchAll() as $line) {
            $entry = new Entry();
            $entry->setId($line['e_id']);
            $entry->setCreatedAt(new DateTime($line['createdAt']));
            $entry->setDescription($line['e_des']);
            $entry->setBabyProfile($this->sqlToBabyProfile($line));
            $entry->setIcon($this->sqlToIcon($line));
            $list[] = $entry;
        }
        return $list;
    } 

    /**
     * @return Entry[]
     */
    public function findByBabyIdAndDate(int $id,  DateTime  $date):?array {
        $list = [];
        $statement = $this->connection->prepare('SELECT *,e.id e_id,e.description e_des, b.id b_id,b.description b_des,b.name b_name, i.id i_id, i.name i_name FROM entry e LEFT JOIN baby_profile b ON e.id_baby = b.id LEFT JOIN icon i ON e.id_icon = i.id WHERE id_baby = :id_baby AND DATE_FORMAT(e.createdAt, \'%Y-%m-%d\') = :date ORDER BY e.createdAt ASC'); 
        $statement->bindValue('id_baby', $id);
        $statement->bindValue('date', $date->format('Y-m-d'));
        $statement->execute();
        foreach($statement->fetchAll() as $line) {
            $entry = new Entry();
            $entry->setId($line['e_id']);
            $entry->setCreatedAt(new DateTime($line['createdAt']));
            $entry->setDescription($line['e_des']);
            $entry->setBabyProfile($this->sqlToBabyProfile($line));
            $entry->setIcon($this->sqlToIcon($line));
            $list[] = $entry;
        }
        return $list;
    } 

    public function findEntryCountByBabyIdAndDate(int $id,  DateTime  $date):?array{
        $list = [];
        $statement = $this->connection->prepare('SELECT e.id_icon As id_icon, i.name AS icon_name, COUNT(e.id_icon) AS entryCount
        FROM entry e
        LEFT JOIN icon i ON e.id_icon = i.id
        WHERE e.id_baby = :id AND DATE_FORMAT(e.createdAt,  \'%Y-%m-%d\') = :date
        GROUP BY e.id_icon;'); 
        $statement->bindValue('id', $id);
        $statement->bindValue('date', $date->format('Y-m-d'));
        $statement->execute();
        foreach ($statement->fetchAll() as $line) {
            $entryCountInfo = [
                'id'=>$line['id_icon'],
                'icon_name' => $line['icon_name'],
                'entryCount' => $line['entryCount'],
            ];
            $list[] = $entryCountInfo;
        }
        return $list;
    }

    public function persist(Entry $entry):void {
        $statement = $this->connection->prepare('INSERT INTO entry (id_icon,id_baby,createdAt,description) VALUES (:id_icon,:id_baby, :createdAt,:description );');
        $statement->bindValue(':id_icon', $entry->getIcon()->getId(),PDO::PARAM_INT);
        $statement->bindValue(':id_baby', $entry->getBabyProfile()->getId(),PDO::PARAM_INT);
        $statement->bindValue(':createdAt', $entry->getCreatedAt()->format('Y-m-d H:i:s'),PDO::PARAM_STR);
        $statement->bindValue(':description', $entry->getDescription(),PDO::PARAM_STR);
        $statement->execute();
        $entry->setId($this->connection->lastInsertId());
    }

    public function update(Entry $entry):void{
        $statement = $this->connection->prepare('UPDATE entry SET createdAt=:createdAt,description=:description, id_icon=:id_icon,id_baby=:id_baby WHERE id=:id;');
        $statement->bindValue(':createdAt', $entry->getCreatedAt()->format('Y-m-d H:i:s'),PDO::PARAM_STR);
        $statement->bindValue(':description', $entry->getDescription(),PDO::PARAM_STR);
        $statement->bindValue(':id_icon', $entry->getIcon()->getId(),PDO::PARAM_INT);
        $statement->bindValue(':id_baby', $entry->getBabyProfile()->getId(),PDO::PARAM_INT);
        $statement->bindValue('id', $entry->getId(), PDO::PARAM_INT);
        $statement->execute();
    }

    public function delete(Entry $entry):void{
        $statement = $this->connection->prepare('DELETE FROM entry WHERE entry.id = :id');
        $statement->bindValue('id', $entry->getId(),PDO::PARAM_INT);
        $statement->execute();
    }

}

