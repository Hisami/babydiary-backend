<?php

namespace App\Repository;
use App\Entities\User;


class UserRepository {
    private $connection;
    public function __construct() {
        $this->connection = Connection::getConnection();;
    } 
    public function persist(User $user):void {
        $statement = $this->connection->prepare('INSERT INTO user (email,password,role, imgUrl,name) VALUES (:email,:password,:role, :imgUrl,:name);');
        $statement->bindValue(':email', $user->getEmail());
        $statement->bindValue(':password', $user->getPassword());
        $statement->bindValue(':role', $user->getRole());
        $statement->bindValue(':imgUrl', $user->getImgUrl()?: null);
        $statement->bindValue(':name', $user->getName()?: null);
        $statement->execute();
        $user->setId($this->connection->lastInsertId());
    }
    
    public function findByEmail(string $email): ?User {
        $statement = $this->connection->prepare('SELECT * FROM user WHERE email=:email');
        $statement->bindValue(':email', $email);
        $statement->execute();
        $result = $statement ->fetch();
        if($result){
            $user = new User();
            $user->setId($result['id']);
            $user->setEmail($result['email']);
            $user->setPassword($result['password']);
            $user->setRole($result['role']);
            $user->setImgUrl($result['imgUrl']);
            $user->setName($result['name']);
        return $user;
        }
        return null;
    }

    public function findById(int $id): ?User{
        $statement = $this->connection->prepare('SELECT * FROM user WHERE id=:id');
        $statement->bindValue(':id', $id);
        $statement->execute();
        $result = $statement ->fetch();
        if($result){
            $user = new User();
            $user->setId($result['id']);
            $user->setEmail($result['email']);
            $user->setPassword($result['password']);
            $user->setRole($result['role']);
            $user->setImgUrl($result['imgUrl']);
            $user->setName($result['name']);
        return $user;
        }
        return null;
    }

    /**
     * @return User[]
     */
    public function findAll():array {
        $list = [];
        $statement = $this->connection->prepare('SELECT * FROM user');
        $statement->execute();
        foreach($statement->fetchAll() as $line) {
            $user = new User();
            $user->setId($line['id']);
            $user->setEmail($line['email']);
            $user->setPassword($line['password']);
            $user->setRole($line['role']);
            $user->setImgUrl($line['imgUrl']);
            $user->setName($line['name']);
            $list[] = $user;
        }
        return $list;
    }

    public function update(User $user): void{
        $statement = $this->connection->prepare('UPDATE user SET email=:email,password=:password,role=:role,imgUrl=:imgUrl,name=:name WHERE id=:id');
        $statement->bindValue(':email', $user->getEmail());
        $statement->bindValue(':password', $user->getPassword());
        $statement->bindValue(':role', $user->getRole());
        $statement->bindValue(':imgUrl', $user->getImgUrl());
        $statement->bindValue(':name', $user->getName());
        $statement->bindValue(':id', $user->getId());
        $statement->execute();
    }
    
}