<?php

namespace App\Repository;

use App\Entities\BabyProfile;
use App\Entities\User;
use DateTime;
use PDO;

class BabyProfileRepository {
    private $connection;
    public function __construct() {
        $this->connection = Connection::getConnection();
    } 
    
    public function sqlToUser(array $result): ?User {
        if($result){
            $user = new User();
            $user->setId($result['u_id']);
            $user->setName($result['u_name']);
            $user->setEmail($result['email']);
            $user->setPassword($result['password']);
            $user->setRole($result['role']);
            $user->setImgUrl($result['u_img']);
            return $user;
        }else{
            return null;
        }
    }
    /**
     * @return BabyProfile[]
     */
    public function findAll():?array {
        $list = [];
        $statement = $this->connection->prepare('SELECT *, b.id b_id, b.name b_name, b.imgUrl b_img, u.id u_id, u.name u_name,u.imgUrl u_img FROM baby_profile b LEFT JOIN user u ON b.id_user = u.id;');
        $statement->execute();
        foreach($statement->fetchAll() as $line) {
            $babyProfile = new BabyProfile();
            $babyProfile->setId($line['b_id']);
            $babyProfile->setName($line['b_name']);
            $babyProfile->setImgUrl($line['b_img']);
            $babyProfile->setBirthDate(new DateTime($line['birthDate']));
            $babyProfile->setDescription($line['description']);
            $babyProfile->setUser($this->sqlToUser($line));
            $list[] = $babyProfile;
        }
        return $list;
    }
    
    /**
     * @return BabyProfile
     */
    public function findById(int $id):?BabyProfile{
    $statement = $this->connection->prepare('SELECT *, b.id b_id, b.name b_name, b.imgUrl b_img, u.id u_id, u.name u_name,u.imgUrl u_img FROM baby_profile b LEFT JOIN user u ON b.id_user = u.id WHERE b.id= :id;');
    $statement->bindValue('id', $id);
    $statement->execute();
    $result = $statement ->fetch();
    if($result){
        $babyProfile = new BabyProfile();
        $babyProfile->setId($result['b_id']);
        $babyProfile->setName($result['b_name']);
        $babyProfile->setImgUrl($result['b_img']);
        $babyProfile->setBirthDate(new DateTime($result['birthDate']));
        $babyProfile->setDescription($result['description']);
        $babyProfile->setUser($this->sqlToUser($result));
        return $babyProfile;
    }
        return null;
    }

    /**
     * @return BabyProfile[]
     */
    public function findByUserId(int $id):?array {
        $list = [];
        $statement = $this->connection->prepare('SELECT *, b.id b_id, b.name b_name, b.imgUrl b_img, u.id u_id, u.name u_name,u.imgUrl u_img FROM baby_profile b LEFT JOIN user u ON b.id_user = u.id WHERE id_user= :id;');
        $statement->bindValue('id', $id);
        $statement->execute();
        foreach($statement->fetchAll() as $line) {
            $babyProfile = new BabyProfile();
            $babyProfile->setId($line['b_id']);
            $babyProfile->setName($line['b_name']);
            $babyProfile->setImgUrl($line['b_img']);
            $babyProfile->setBirthDate(new DateTime($line['birthDate']));
            $babyProfile->setDescription($line['description']);
            $babyProfile->setUser($this->sqlToUser($line));
            $list[] = $babyProfile;
        }
        return $list;
    } 

    public function persist(BabyProfile $babyProfile):void {
        $statement = $this->connection->prepare('INSERT INTO baby_profile (name,birthDate,imgUrl,description,id_user) VALUES (:name,:birthDate,:imgUrl, :description,:id_user)');
        $statement->bindValue(':name', $babyProfile->getName(),PDO::PARAM_STR);
        $statement->bindValue(':birthDate', $babyProfile->getBirthDate()->format('Y-m-d'),PDO::PARAM_STR);
        $statement->bindValue(':imgUrl', $babyProfile->getImgUrl(),PDO::PARAM_STR);
        $statement->bindValue(':description', $babyProfile->getDescription(),PDO::PARAM_STR);
        $statement->bindValue(':id_user', $babyProfile->getUser()->getId(),PDO::PARAM_INT);
        $statement->execute();
        $babyProfile->setId($this->connection->lastInsertId());
    }

    public function update(BabyProfile $babyProfile):void{
        $statement = $this->connection->prepare('UPDATE baby_profile SET name=:name,imgUrl=:imgUrl,birthDate=:birthDate,description=:description,id_user=:id_user WHERE id=:id;');
        $statement->bindValue(':name', $babyProfile->getName(),PDO::PARAM_STR);
        $statement->bindValue(':imgUrl', $babyProfile->getImgUrl(),PDO::PARAM_STR);
        $statement->bindValue(':birthDate', $babyProfile->getBirthDate()->format('Y-m-d'),PDO::PARAM_STR);
        $statement->bindValue(':description', $babyProfile->getDescription(),PDO::PARAM_STR);
        $statement->bindValue(':id_user', $babyProfile->getUser()->getId(),PDO::PARAM_INT);
        $statement->bindValue('id', $babyProfile->getId(), PDO::PARAM_INT);
        $statement->execute();
    }

    public function delete(BabyProfile $babyProfile):void{
        $statement = $this->connection->prepare('DELETE FROM baby_profile WHERE baby_profile.id = :id');
        $statement->bindValue('id', $babyProfile->getId(),PDO::PARAM_INT);
        $statement->execute();
    }

}

