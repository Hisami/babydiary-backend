<?php

namespace App\Repository;
use App\Entities\Comment;
use App\Entities\BabyProfile;
use DateTime;
use PDO;

class CommentRepository {
    private $connection;
    public function __construct() {
        $this->connection = Connection::getConnection();
    } 

    public function sqlToBabyProfile(array $result): ?BabyProfile {
        if($result){
            $babyProfile = new BabyProfile();
            $babyProfile->setId($result['b_id']);
            $babyProfile->setName($result['name']);
            $babyProfile->setImgUrl($result['b_img']);
            $babyProfile->setBirthDate(new DateTime($result['birthDate']));
            $babyProfile->setDescription($result['description']);
            return $babyProfile;
        }else{
            return null;
        }
    }

    /**
     * @return Comment[]
     */
    public function findAll():?array {
        $list = [];
        $statement = $this->connection->prepare('SELECT *,c.id c_id, c.imgUrl c_img,b.id b_id, b.imgUrl b_img FROM comment c LEFT JOIN baby_profile b ON c.id_baby = b.id;');
        $statement->execute();
        foreach($statement->fetchAll() as $line) {
            $comment = new Comment();
            $comment->setId($line['c_id']);
            $comment->setCreatedAt(new DateTime($line['createdAt']));
            $comment->setImgUrl($line['c_img']);
            $comment->setComment($line['comment']);
            $comment->setBabyProfile($this->sqlToBabyProfile($line));
            $list[] = $comment;
        }
        return $list;
    }
    
    /**
     * @return Comment
     */
    public function findById(int $id):?Comment{
    $statement = $this-> connection->prepare('SELECT *,c.id c_id, c.imgUrl c_img,b.id b_id, b.imgUrl b_img FROM comment c LEFT JOIN baby_profile b ON c.id_baby = b.id WHERE c.id= :id;');
    $statement->bindValue('id', $id);
    $statement->execute();
    $result = $statement ->fetch();
    if($result){
        $comment = new Comment();
        $comment->setId($result['c_id']);
        $comment->setCreatedAt(new DateTime($result['createdAt']));
        $comment->setImgUrl($result['c_img']);
        $comment->setComment($result['comment']);
        $comment->setBabyProfile($this->sqlToBabyProfile($result));
        return $comment;
    }
        return null;
    }

    /**
     * @return Comment[]
     */
    public function findByBabyId(int $id):?array {
        $list = [];
        $statement = $this->connection->prepare('SELECT *,c.id c_id, c.imgUrl c_img,b.id b_id, b.imgUrl b_img FROM comment c LEFT JOIN baby_profile b ON c.id_baby = b.id WHERE id_baby= :id;');
        $statement->bindValue('id', $id);
        $statement->execute();
        foreach($statement->fetchAll() as $line) {
            $comment = new Comment();
            $comment->setId($line['c_id']);
            $comment->setCreatedAt(new DateTime($line['createdAt']));
            $comment->setImgUrl($line['c_img']);
            $comment->setComment($line['comment']);
            $comment->setBabyProfile($this->sqlToBabyProfile($line));
            $list[] = $comment;
        }
        return $list;
    } 

    public function findCommentByBabyIdAndDate(int $id, DateTime $date):?array{
        $list = [];
        $statement = $this->connection->prepare('SELECT *,c.id c_id, c.imgUrl c_img,b.id b_id, b.imgUrl b_img FROM comment c LEFT JOIN baby_profile b ON c.id_baby = b.id WHERE id_baby =:id_baby  AND DATE_FORMAT(c.createdAt, \'%Y-%m-%d\') = :date  ORDER BY c.createdAt ASC;');
        $statement->bindValue('id_baby', $id);
        $statement->bindValue('date', $date->format('Y-m-d'));
        $statement->execute();
        foreach($statement->fetchAll() as $line) {
            $comment = new Comment();
            $comment->setId($line['c_id']);
            $comment->setCreatedAt(new DateTime($line['createdAt']));
            $comment->setImgUrl($line['c_img']);
            $comment->setComment($line['comment']);
            $comment->setBabyProfile($this->sqlToBabyProfile($line));
            $list[] = $comment;
        }
        return $list;
    }

    public function persist(Comment $comment):void {
        $statement = $this->connection->prepare('INSERT INTO comment (createdAt,imgUrl,comment,id_baby) VALUES (:createdAt,:imgUrl, :comment,:id_baby);');
        $statement->bindValue(':createdAt', $comment->getCreatedAt()->format('Y-m-d H:i:s'),PDO::PARAM_STR);
        $statement->bindValue(':imgUrl', $comment->getImgUrl(),PDO::PARAM_STR);
        $statement->bindValue(':comment', $comment->getComment(),PDO::PARAM_STR);
        $statement->bindValue(':id_baby', $comment->getBabyProfile()->getId(),PDO::PARAM_INT);
        $statement->execute();
        $comment->setId($this->connection->lastInsertId());
    }

    public function update(Comment $comment):void{
        $statement = $this->connection->prepare('UPDATE comment SET createdAt=:createdAt,imgUrl=:imgUrl,comment=:comment,id_baby=:id_baby WHERE id=:id;');
        $statement->bindValue(':createdAt', $comment->getCreatedAt()->format('Y-m-d H:i:s'),PDO::PARAM_STR);
        $statement->bindValue(':imgUrl', $comment->getImgUrl(),PDO::PARAM_STR);
        $statement->bindValue(':comment', $comment->getComment(),PDO::PARAM_STR);
        $statement->bindValue(':id_baby', $comment->getBabyProfile()->getId(),PDO::PARAM_INT);
        $statement->bindValue('id', $comment->getId(), PDO::PARAM_INT);
        $statement->execute();
    }

    public function delete(Comment $comment):void{
        $statement = $this->connection->prepare('DELETE FROM comment WHERE comment.id = :id');
        $statement->bindValue('id', $comment->getId(),PDO::PARAM_INT);
        $statement->execute();
    }

}

