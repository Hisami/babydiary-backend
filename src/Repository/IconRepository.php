<?php

namespace App\Repository;

use App\Entities\Icon;
use PDO;

class IconRepository {
    private $connection;

    public function __construct() {
        $this->connection = Connection::getConnection();
    } 

    /**
     * @return Icon[]
     */
    public function findAll():?array {
        $list = [];
        $statement = $this->connection->prepare('SELECT * FROM icon;');
        $statement->execute();
        foreach($statement->fetchAll() as $line) {
            $icon = new Icon();
            $icon->setId($line['id']);
            $icon->setName($line['name']);
            $icon->setExplication($line['explication']);
            $list[] = $icon;
        }
        return $list;
    }
    
    /**
     * @return Icon
     */
    public function findById(int $id):?Icon{
    $statement = $this-> connection->prepare('SELECT * FROM icon WHERE id= :id;');
    $statement->bindValue('id', $id);
    $statement->execute();
    $result = $statement ->fetch();
    if($result){
        $icon = new Icon();
        $icon->setId($result['id']);
        $icon->setName($result['name']);
        $icon->setExplication($result['explication']);
        return $icon;
    }
        return null;
    }

    public function persist(Icon $icon):void {
        $statement = $this->connection->prepare('INSERT INTO icon (name, explication) VALUES (:name,:explication);');
        $statement->bindValue(':name', $icon->getName(),PDO::PARAM_STR);
        $statement->bindValue(':explication', $icon->getExplication(),PDO::PARAM_STR);
        $statement->execute();
        $icon->setId($this->connection->lastInsertId());
    }

    public function update(Icon $icon):void{
        $statement = $this->connection->prepare('UPDATE icon SET name=:name,explication=:explication WHERE id=:id;');
        $statement->bindValue(':name', $icon->getName(),PDO::PARAM_STR);
        $statement->bindValue(':explication', $icon->getExplication(),PDO::PARAM_STR);
        $statement->bindValue('id', $icon->getId(), PDO::PARAM_INT);
        $statement->execute();
    }

    public function delete(Icon $icon):void{
        $statement = $this->connection->prepare('DELETE FROM icon WHERE id = :id');
        $statement->bindValue('id', $icon->getId(),PDO::PARAM_INT);
        $statement->execute();
    }

}

