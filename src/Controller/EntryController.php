<?php

namespace App\Controller;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use App\Entities\Entry;
use App\Repository\EntryRepository;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\SerializerInterface;
use DateTime;

#[Route('/api/entry')]
class EntryController extends AbstractController {
    public function __construct(private EntryRepository $eRepo) {}

    #[Route(methods: 'GET')]
    public function all() : JsonResponse{
        try{
        return $this->json($this->eRepo->findAll());
        }catch (\Exception $e) {
            return new JsonResponse(['error' => $e->getMessage()], JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    #[Route('/{id}',methods: 'GET')]
    public function find(int $id) : JsonResponse{
        $entry = $this->eRepo->findById($id);
        if(!$entry){
            throw new NotFoundHttpException();
        }
        return $this->json($entry);
    }

    #[Route('/babyProfile/{id}',methods: 'GET')]
    public function findByBaby(int $id) : JsonResponse{
        $entry = $this->eRepo->findByBabyId($id);
        if(!$entry){
            throw new NotFoundHttpException();
        }
        return $this->json($entry);
    }

    #[Route('/babyProfile/{id}/date/{date}',methods: 'GET')]
    public function findByBabyandDate(int $id,string $date) : JsonResponse{
        $dateObject = new DateTime($date);
        $entry = $this->eRepo->findByBabyIdAndDate($id,$dateObject);
        if(!$entry){
            throw new NotFoundHttpException();
        }
        return $this->json($entry);
    }

    #[Route('/babyProfile/{id}/date/{date}/entryCount',methods: 'GET')]
    public function findEntryCount(int $id,string $date) : JsonResponse{
        $dateObject = new DateTime($date);
        $entry = $this->eRepo->findEntryCountByBabyIdAndDate($id,$dateObject);
        if(!$entry){
            throw new NotFoundHttpException();
        }
        return $this->json($entry);
    }

    #[Route(methods: 'POST')]
    public function add(Request $request, SerializerInterface $serializer): JsonResponse {
        try{
            $entry = $serializer->deserialize($request->getContent(), Entry::class, 'json');
            }catch(\Exception $e){
            $errorMessage = $e->getMessage();
            return $this->json($errorMessage, 400);
            }
        $this->eRepo->persist($entry);
        return $this->json($entry, Response::HTTP_CREATED);
}

    #[Route('/{id}', methods:'PUT')]
    public function put(int $id, Request $request, SerializerInterface $serializer):JsonResponse{
        $entry = $this->eRepo->findById($id);
        if(!$entry){
            throw new NotFoundHttpException();
        }
        $toUpdate = $serializer->deserialize($request->getContent(), Entry::class,'json');
        $toUpdate->setId($id);
        $this->eRepo->update($toUpdate);
        return $this->json($toUpdate);
    }

    #[Route('/{id}', methods: 'DELETE')]
    public function remove(int $id) {   
        $entry = $this->eRepo->findById($id);
        if(!$entry){
            throw new NotFoundHttpException();
        }
        $this->eRepo->delete($entry);
        return $this->json(null, Response::HTTP_NO_CONTENT);
}

}
