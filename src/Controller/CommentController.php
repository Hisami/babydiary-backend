<?php

namespace App\Controller;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use App\Entities\Comment;
use App\Entities\BabyProfile;
use App\Service\Uploader;
use App\Repository\CommentRepository;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use DateTime;
#[Route('/api/comment')]
class CommentController extends AbstractController {
    public function __construct(private CommentRepository $cRepo) {}

    #[Route(methods: 'GET')]
    public function all() : JsonResponse{
        try{
        return $this->json($this->cRepo->findAll());
        }catch (\Exception $e) {
            return new JsonResponse(['error' => $e->getMessage()], JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    #[Route('/{id}',methods: 'GET')]
    public function find(int $id) : JsonResponse{
        $comment = $this->cRepo->findById($id);
        if(!$comment){
            throw new NotFoundHttpException();
        }
        return $this->json($comment);
    }

    #[Route('/babyProfile/{id}',methods: 'GET')]
    public function findByBaby(int $id) : JsonResponse{
        $comment = $this->cRepo->findByBabyId($id);
        if(!$comment){
            throw new NotFoundHttpException();
        }
        return $this->json($comment);
    }

    #[Route('/babyProfile/{id}/date/{date}',methods:'GET')]
    public function findByBabyAndDate(int $id, string $date):JsonResponse{
    $dateObject = new DateTime($date);
    $comment = $this->cRepo->findCommentByBabyIdAndDate($id, $dateObject);
    if(!$comment){
        throw new NotFoundHttpException();
    }
    return $this->json($comment);
    }

    #[Route(methods: 'POST')]
    public function add(Request $request, SerializerInterface $serializer ,ValidatorInterface $validator,Uploader $uploader): JsonResponse {
        try{
            $comment = $serializer->deserialize($request->getContent(), Comment::class, 'json');
            }catch(\Exception $e){
            $errorMessage = $e->getMessage();
            return $this->json($errorMessage, 400);
            }
        $errors = $validator->validate($comment);
            if($errors->count() > 0) {
                return $this->json($errors, 400);
            }
        $comment->setImgUrl($uploader->upload($comment->getImgUrl()));
        $this->cRepo->persist($comment);
        return $this->json($comment, Response::HTTP_CREATED);
}

    #[Route('/{id}', methods:'PUT')]
    public function put(int $id, Request $request, SerializerInterface $serializer):JsonResponse{
        $comment = $this->cRepo->findById($id);
        if(!$comment){
            throw new NotFoundHttpException();
        }
        $toUpdate = $serializer->deserialize($request->getContent(), Comment::class,'json');
        $toUpdate->setId($id);
        $this->cRepo->update($toUpdate);
        return $this->json($toUpdate);
    }

    #[Route('/{id}', methods: 'DELETE')]
    public function remove(int $id) {   
        $comment = $this->cRepo->findById($id);
        if(!$comment){
            throw new NotFoundHttpException();
        }
        $this->cRepo->delete($comment);
        return $this->json(null, Response::HTTP_NO_CONTENT);
}

}
