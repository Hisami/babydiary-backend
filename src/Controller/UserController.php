<?php

namespace App\Controller;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use App\Entities\User;
use App\Repository\UserRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Serializer\SerializerInterface;

#[Route('/api/user')]
class UserController extends AbstractController {
    public function __construct(private UserRepository $uRepo) {}

    #[Route(methods:'GET')]
    public function all(): JsonResponse {
        try{
            return $this->json($this->uRepo->findAll());
        }catch (\Exception $e) {
            return new JsonResponse(['error' => $e->getMessage()], JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    #[Route('/{id}',methods: 'GET')]
    public function find(int $id) : JsonResponse{
        $babyProfile = $this->uRepo->findById($id);
        if(!$babyProfile){
            throw new NotFoundHttpException();
        }
        return $this->json($babyProfile);
    }

    #[Route('/{id}/promote', methods: 'PATCH')]
    public function promote(int $id, Request $request): JsonResponse
    {
        $user = $this->uRepo->findById($id);
        if(!$user) {
            throw new NotFoundHttpException('User does not exist');
        }
        $user->setRole('ROLE_ADMIN');
        $this->uRepo->update($user);
        
        return $this->json($user);

    }

    #[Route('/{id}', methods:'PUT')]
    public function put(int $id, Request $request, SerializerInterface $serializer):JsonResponse{
        $userProfile = $this->uRepo->findById($id);
        if(!$userProfile){
            throw new NotFoundHttpException();
        }
        $toUpdate = $serializer->deserialize($request->getContent(), User::class,'json');
        $toUpdate->setId($id);
        $this->uRepo->update($toUpdate);
        return $this->json($toUpdate);
    }
    

    

}
