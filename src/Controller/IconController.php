<?php

namespace App\Controller;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use App\Entities\Icon;
use App\Repository\IconRepository;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\SerializerInterface;

#[Route('/api/icon')]
class IconController extends AbstractController {
    public function __construct(private IconRepository $iRepo) {}

    #[Route(methods: 'GET')]
    public function all() : JsonResponse{
        try{
        return $this->json($this->iRepo->findAll());
        }catch (\Exception $e) {
            return new JsonResponse(['error' => $e->getMessage()], JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    #[Route('/{id}',methods: 'GET')]
    public function find(int $id) : JsonResponse{
        $icon = $this->iRepo->findById($id);
        if(!$icon){
            throw new NotFoundHttpException();
        }
        return $this->json($icon);
    }

    #[Route(methods: 'POST')]
    public function add(Request $request, SerializerInterface $serializer): JsonResponse {
        try{
            $icon = $serializer->deserialize($request->getContent(), Icon::class, 'json');
            }catch(\Exception $e){
            $errorMessage = $e->getMessage();
            return $this->json($errorMessage, 400);
            }
        $this->iRepo->persist($icon);
        return $this->json($icon, Response::HTTP_CREATED);
}

    #[Route('/{id}', methods:'PUT')]
    public function put(int $id, Request $request, SerializerInterface $serializer):JsonResponse{
        $icon = $this->iRepo->findById($id);
        if(!$icon){
            throw new NotFoundHttpException();
        }
        $toUpdate = $serializer->deserialize($request->getContent(), Icon::class,'json');
        $toUpdate->setId($id);
        $this->iRepo->update($toUpdate);
        return $this->json($toUpdate);
    }

    #[Route('/{id}', methods: 'DELETE')]
    public function remove(int $id) {   
        $icon = $this->iRepo->findById($id);
        if(!$icon){
            throw new NotFoundHttpException();
        }
        $this->iRepo->delete($icon);
        return $this->json(null, Response::HTTP_NO_CONTENT);
}

}
