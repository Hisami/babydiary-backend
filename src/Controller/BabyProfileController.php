<?php

namespace App\Controller;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use App\Entities\BabyProfile;
use App\Repository\BabyProfileRepository;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\SerializerInterface;

#[Route('/api/babyProfile')]
class BabyProfileController extends AbstractController {
    public function __construct(private BabyProfileRepository $bpRepo) {}

    #[Route(methods: 'GET')]
    public function all() : JsonResponse{
        try{
        return $this->json($this->bpRepo->findAll());
        }catch (\Exception $e) {
            return new JsonResponse(['error' => $e->getMessage()], JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    #[Route('/{id}',methods: 'GET')]
    public function find(int $id) : JsonResponse{
        $babyProfile = $this->bpRepo->findById($id);
        if(!$babyProfile){
            throw new NotFoundHttpException();
        }
        return $this->json($babyProfile);
    }

    #[Route('/user/{id}',methods: 'GET')]
    public function findByUser(int $id) : JsonResponse{
        $babyProfile = $this->bpRepo->findByUserId($id);
        if(!$babyProfile){
            throw new NotFoundHttpException();
        }
        return $this->json($babyProfile);
    }

    #[Route(methods: 'POST')]
    public function add(Request $request, SerializerInterface $serializer): JsonResponse {
        try{
            $babyProfile = $serializer->deserialize($request->getContent(), BabyProfile::class, 'json');
            }catch(\Exception $e){
            $errorMessage = $e->getMessage();
            return $this->json($errorMessage, 400);
            }
        $this->bpRepo->persist($babyProfile);
        return $this->json($babyProfile, Response::HTTP_CREATED);
}

    #[Route('/{id}', methods:'PUT')]
    public function put(int $id, Request $request, SerializerInterface $serializer):JsonResponse{
        $babyProfile = $this->bpRepo->findById($id);
        if(!$babyProfile){
            throw new NotFoundHttpException();
        }
        $toUpdate = $serializer->deserialize($request->getContent(), BabyProfile::class,'json');
        $toUpdate->setId($id);
        $this->bpRepo->update($toUpdate);
        return $this->json($toUpdate);
    }

    #[Route('/{id}', methods: 'DELETE')]
    public function remove(int $id) {   
        $babyProfile = $this->bpRepo->findById($id);
        if(!$babyProfile){
            throw new NotFoundHttpException();
        }
        $this->bpRepo->delete($babyProfile);
        return $this->json(null, Response::HTTP_NO_CONTENT);
}

}
