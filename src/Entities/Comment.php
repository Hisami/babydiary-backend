<?php

namespace App\Entities;
use DateTime;
use App\Entities\BabyProfile;
use Symfony\Component\Validator\Constraints as Assert;

class Comment{
    private ?int $id;
	#[Assert\NotBlank]
    private BabyProfile $babyProfile;
    #[Assert\NotBlank]
    private datetime $createdAt;
    private ?string $imgUrl;
    private ?string $comment;

	/**
	 * @return int|null
	 */
	public function getId(): ?int {
		return $this->id;
	}
	
	/**
	 * @param int|null $id 
	 * @return self
	 */
	public function setId(?int $id): self {
		$this->id = $id;
		return $this;
	}
	
	/**
	 * @return BabyProfile
	 */
	public function getBabyProfile(): BabyProfile {
		return $this->babyProfile;
	}
	
	/**
	 * @param BabyProfile $babyProfile 
	 * @return self
	 */
	public function setBabyProfile(BabyProfile $babyProfile): self {
		$this->babyProfile = $babyProfile;
		return $this;
	}
	
	/**
	 * @return DateTime
	 */
	public function getCreatedAt(): DateTime {
		return $this->createdAt;
	}
	
	/**
	 * @param DateTime $createdAt 
	 * @return self
	 */
	public function setCreatedAt(DateTime $createdAt): self {
		$this->createdAt = $createdAt;
		return $this;
	}
	
	/**
	 * @return string|null
	 */
	public function getImgUrl(): ?string {
		return $this->imgUrl;
	}
	
	/**
	 * @param string|null $imgUrl 
	 * @return self
	 */
	public function setImgUrl(?string $imgUrl): self {
		$this->imgUrl = $imgUrl;
		return $this;
	}
	
	/**
	 * @return string|null
	 */
	public function getComment(): ?string {
		return $this->comment;
	}
	
	/**
	 * @param string|null $comment 
	 * @return self
	 */
	public function setComment(?string $comment): self {
		$this->comment = $comment;
		return $this;
	}
}