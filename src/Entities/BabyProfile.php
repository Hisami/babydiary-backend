<?php

namespace App\Entities;
use DateTime;
use App\Entities\User;
use Symfony\Component\Validator\Constraints as Assert;

class BabyProfile{
    private ?int $id;
    #[Assert\NotBlank]
    private string $name;
    #[Assert\NotBlank]
    private datetime $birthDate;
    private ?string $imgUrl;
    private ?string $description;
    private User $user;

	/**
	 * @return int|null
	 */
	public function getId(): ?int {
		return $this->id;
	}
	
	/**
	 * @param int|null $id 
	 * @return self
	 */
	public function setId(?int $id): self {
		$this->id = $id;
		return $this;
	}
	
	/**
	 * @return string
	 */
	public function getName(): string {
		return $this->name;
	}
	
	/**
	 * @param string $name 
	 * @return self
	 */
	public function setName(string $name): self {
		$this->name = $name;
		return $this;
	}
	
	/**
	 * @return DateTime
	 */
	public function getBirthDate(): DateTime {
		return $this->birthDate;
	}
	
	/**
	 * @param DateTime $birthDate 
	 * @return self
	 */
	public function setBirthDate(DateTime $birthDate): self {
		$this->birthDate = $birthDate;
		return $this;
	}
	
	/**
	 * @return string|null
	 */
	public function getImgUrl(): ?string {
		return $this->imgUrl;
	}
	
	/**
	 * @param string|null $imgUrl 
	 * @return self
	 */
	public function setImgUrl(?string $imgUrl): self {
		$this->imgUrl = $imgUrl;
		return $this;
	}
	
	/**
	 * @return string|null
	 */
	public function getDescription(): ?string {
		return $this->description;
	}
	
	/**
	 * @param string|null $description 
	 * @return self
	 */
	public function setDescription(?string $description): self {
		$this->description = $description;
		return $this;
	}
	
	/**
	 * @return User
	 */
	public function getUser(): User {
		return $this->user;
	}
	
	/**
	 * @param User $user 
	 * @return self
	 */
	public function setUser(User $user): self {
		$this->user = $user;
		return $this;
	}
}