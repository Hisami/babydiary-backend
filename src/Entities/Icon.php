<?php

namespace App\Entities;
use Symfony\Component\Validator\Constraints as Assert;

class Icon{
    private ?int $id;
	#[Assert\NotBlank]
    private string $name;
    #[Assert\NotBlank]
    private string $explication;
    
	/**
	 * @return int|null
	 */
	public function getId(): ?int {
		return $this->id;
	}
	
	/**
	 * @param int|null $id 
	 * @return self
	 */
	public function setId(?int $id): self {
		$this->id = $id;
		return $this;
	}
	
	/**
	 * @return string
	 */
	public function getName(): string {
		return $this->name;
	}
	
	/**
	 * @param string $name 
	 * @return self
	 */
	public function setName(string $name): self {
		$this->name = $name;
		return $this;
	}
	
	/**
	 * @return string
	 */
	public function getExplication(): string {
		return $this->explication;
	}
	
	/**
	 * @param string $explication 
	 * @return self
	 */
	public function setExplication(string $explication): self {
		$this->explication = $explication;
		return $this;
	}
}