<?php

namespace App\Entities;
use DateTime;
use App\Entities\BabyProfile;
use App\Entities\Icon;
use Symfony\Component\Validator\Constraints as Assert;

class Entry{
    private ?int $id;
	#[Assert\NotBlank]
    private BabyProfile $babyProfile;
	#[Assert\NotBlank]
    private Icon $icon;
    #[Assert\NotBlank]
    private datetime $createdAt;
    private ?string $description;

	/**
	 * @return int|null
	 */
	public function getId(): ?int {
		return $this->id;
	}
	
	/**
	 * @param int|null $id 
	 * @return self
	 */
	public function setId(?int $id): self {
		$this->id = $id;
		return $this;
	}
	
	/**
	 * @return BabyProfile
	 */
	public function getBabyProfile(): BabyProfile {
		return $this->babyProfile;
	}
	
	/**
	 * @param BabyProfile $babyProfile 
	 * @return self
	 */
	public function setBabyProfile(BabyProfile $babyProfile): self {
		$this->babyProfile = $babyProfile;
		return $this;
	}
	
	/**
	 * @return Icon
	 */
	public function getIcon(): Icon {
		return $this->icon;
	}
	
	/**
	 * @param Icon $icon 
	 * @return self
	 */
	public function setIcon(Icon $icon): self {
		$this->icon = $icon;
		return $this;
	}
	
	/**
	 * @return DateTime
	 */
	public function getCreatedAt(): DateTime {
		return $this->createdAt;
	}
	
	/**
	 * @param DateTime $createdAt 
	 * @return self
	 */
	public function setCreatedAt(DateTime $createdAt): self {
		$this->createdAt = $createdAt;
		return $this;
	}
	
	/**
	 * @return string|null
	 */
	public function getDescription(): ?string {
		return $this->description;
	}
	
	/**
	 * @param string|null $description 
	 * @return self
	 */
	public function setDescription(?string $description): self {
		$this->description = $description;
		return $this;
	}
}